//
//  main.m
//  Matchismo
//
//  Created by Joseph Kandi on 2013/03/18.
//  Copyright (c) 2013 Keep Learning. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KPLCardGameAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KPLCardGameAppDelegate class]));
    }
}
