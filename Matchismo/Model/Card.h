//
//  Card.h
//  Matchismo
//
//  Created by Joseph Kandi on 2013/03/20.
//  Copyright (c) 2013 Keep Learning. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
@property (strong, nonatomic) NSString *contents;
@property (nonatomic, getter = isFaceUp) BOOL faceUp;
@property (nonatomic, getter = isUnPlayable) BOOL unPlayable;
-(int) match:(NSArray *) otherCards;
@end
