//
//  Deck.h
//  Matchismo
//
//  Created by Joseph Kandi on 2013/03/20.
//  Copyright (c) 2013 Keep Learning. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject
-(void)addCard:(Card *) card atTop:(BOOL) atTop;
-(Card *)drawRandomCard;
@end
