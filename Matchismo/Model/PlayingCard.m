//
//  PlayingCard.m
//  Matchismo
//
//  Created by Joseph Kandi on 2013/03/20.
//  Copyright (c) 2013 Keep Learning. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard
@synthesize suit = _suit;
-(NSString *) contents
{
//    switch (self.rank) {
//        case 1:
//            return [NSString stringWithFormat:@"%@%@", @"A", self.suit];
//            break;
//        case 11:
//            return [NSString stringWithFormat:@"%@%@", @"J", self.suit];
//            break;
//        case 12:
//            return [NSString stringWithFormat:@"%@%@", @"Q", self.suit];
//            break;
//        case 13:
//            return [NSString stringWithFormat:@"%@%@", @"K", self.suit];
//            break;
//        default:
//            return [NSString stringWithFormat:@"%d%@", self.rank, self.suit];
//            break;
//    }
    NSArray *rankStrings = [[self class] rankStrings];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}
+(NSArray *) validSuits
{
    static NSArray *validSuits = nil;
    if (!validSuits) {
        validSuits = @[@"♥", @"♦",@"♠",@"♣"];
    }
    return  validSuits;
}
-(void) setSuit:(NSString *)suit
{
    if ([[[self class] validSuits] containsObject:suit]) {
        _suit = suit;
    }
}
-(NSString *) suit
{
    return _suit ? _suit : @"?";
}
+ (NSArray *) rankStrings
{
    static NSArray *rankStrings = nil;
    if (!rankStrings) {
        rankStrings = @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];

    }
    return rankStrings;
}
+(NSUInteger)maxRank
{
    return [self rankStrings].count - 1;
}
-(void)setRank:(NSUInteger)rank
{
    if(rank <= [[self class] maxRank])
    {
        _rank = rank;
    }
}
@end

