//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by Joseph Kandi on 2013/03/20.
//  Copyright (c) 2013 Keep Learning. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
