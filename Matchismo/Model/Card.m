//
//  Card.m
//  Matchismo
//
//  Created by Joseph Kandi on 2013/03/20.
//  Copyright (c) 2013 Keep Learning. All rights reserved.
//

#import "Card.h"

@implementation Card
-(int) match:(NSArray *)otherCards
{
    __block int score = 0;
    [otherCards enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        if ([obj isKindOfClass:[Card class]]) {
        Card *otherCard = (Card *) obj;
        if ([self.contents isEqualToString:otherCard.contents]) {
            score = 1;
        }}
    }];
    return score;
}
@end
