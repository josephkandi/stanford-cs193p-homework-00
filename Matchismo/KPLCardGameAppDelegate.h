//
//  KPLCardGameAppDelegate.h
//  Matchismo
//
//  Created by Joseph Kandi on 2013/03/18.
//  Copyright (c) 2013 Keep Learning. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPLCardGameAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
