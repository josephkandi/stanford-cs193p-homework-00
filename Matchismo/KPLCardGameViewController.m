//
//  KPLCardGameViewController.m
//  Matchismo
//
//  Created by Joseph Kandi on 2013/03/18.
//  Copyright (c) 2013 Keep Learning. All rights reserved.
//

#import "KPLCardGameViewController.h"
#import "PlayingCardDeck.h"

@interface KPLCardGameViewController ()
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (strong, nonatomic) PlayingCardDeck *deck;
@property (nonatomic) int flipCount;
@end

@implementation KPLCardGameViewController

-(PlayingCardDeck *) deck
{
    if (!_deck) {
        _deck = [[PlayingCardDeck alloc] init];
    }
    return _deck;
}
-(void)setFlipCount:(int)flipCount
{
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
}
- (IBAction)flipCard:(UIButton *)sender
{
    //sender.selected = !sender.isSelected; //My code sender.selected = !sender.selected or sender.selected = ![sender isSelected];
    if (!sender.isSelected) {
        sender.selected = YES;
        [sender setTitle:[self.deck drawRandomCard].contents forState:UIControlStateSelected];
        self.flipCount++;
    }else{
        sender.selected = NO;
    }
}

@end
